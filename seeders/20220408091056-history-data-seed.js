'use strict';

const historyData = require('../masterdata/histories.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const historyDataTimestamped = historyData.map((e)=> {
      e['createdAt'] = new Date();
      e['updatedAt'] = new Date();
      return e
    })
    await queryInterface.bulkInsert('Histories', historyDataTimestamped)
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Histories', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
