const { History } = require('../models');
const port = process.env.PORT;
const { validationResult, check } = require('express-validator')

const getAllHistories = async(req, res) => {
    try{
        let { page, row } = req.query;
        if(row == 0 || !page || !row){
            page = 1;
            row = 5;
        }
        page -= page > 0 ? 1 : 0;
        page *= row;
        const optionAll = {
            attributes : ['id', 'user_id', 'match_duration_in_minutes', 'match_score'],
            order : [['id', 'ASC']]
        }
        
        const allHistories = await History.findAll(optionAll);
        const pageCount = Math.ceil(allHistories.length / row);
        
        const options = {
            attributes : ['id', 'user_id', 'match_duration_in_minutes', 'match_score'],
            order : [['id', 'ASC']],
            limit : row,
            offset : page
        }
        const someHistories = await History.findAll(options);
        
        res.render('histories/dashboardHistory', {
            status : 'success',
            histories : someHistories,
            page, 
            row,
            pageCount,
            port
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get all histories');
    }
}

const addNewHistory = (req, res) => {
    try{
        res.render('histories/addNewHistory');
    }catch(err){
        console.log(err);
        throw new Error('cannot route to addNewHistory');
    }
}

const getHistoryById = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            where : {
                id : id
            }
        }
        const history = await History.findOne(options);
        res.render('histories/getHistoryById', {
            history
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get history by Id');
    }
}

const createHistory= async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('histories/addNewHistory', {
                errors : errors.array()
            });
        }else{
            const { user_id, match_duration_in_minutes, match_score } = req.body;
            const option = {
                where : {
                    id : user_id
                }
            }
            const historyById = await History.findAll(option);
            if(historyById.length == 0) {
                res.redirect(400,'/histories');
            }else{
                await History.create({
                    user_id,
                    match_duration_in_minutes,
                    match_score,
                    createdAt : new Date(),
                    updatedAt : new Date()
                });
                res.redirect('/histories');
            }
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot create history');
    }
}

const editHistory = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            attributes : ['id', 'user_id', 'match_duration_in_minutes', 'match_score'],
            where : {
                id : id
            }
        }
        const history = await History.findOne(options);
        res.render('histories/updateHistory', {
            status : 'success',
            history
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot go to editHistory');
    }

}

const updateHistory = async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const { id } = req.params;
            const options = {
                attributes : ['id', 'user_id', 'match_duration_in_minutes', 'match_score'],
                where : {
                    id : id
                }
            }
            const history = await History.findOne(options);
            res.render('histories/updateHistory', {
                errors : errors.array(),
                history
            });
        }else{
            const { match_duration_in_minutes, match_score } = req.body;
            await History.update(
                {
                    match_duration_in_minutes,
                    match_score,
                    updatedAt : new Date()
                },
                {
                    where : { id : req.params.id }
                }
            );
            res.redirect('/histories');
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot update history');
    }
}

const deleteHistory = async(req, res) => {
    try{
        const { id } = req.params;
        await History.destroy({
            where : {
                id : id
            }
        })
        res.redirect('/histories');
    }catch(err){
        console.log(err);
        throw new Error('cannot delete history');
    }
}

module.exports = {
    getAllHistories,
    addNewHistory,
    getHistoryById,
    createHistory,
    editHistory,
    updateHistory,
    deleteHistory
}

