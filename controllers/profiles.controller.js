const { Profile } = require('../models');
const port = process.env.PORT;
const { validationResult, check } = require('express-validator')

const getAllProfiles = async(req, res) => {
    try{
        let { page, row } = req.query;
        if(row == 0 || !page || !row){
            page = 1;
            row = 5;
        }
        page -= page > 0 ? 1 : 0;
        page *= row;
        const optionAll = {
            attributes : ['id', 'user_id', 'user_country', 'user_rank', 'gold_amount'],
            order : [['id', 'ASC']]
        }
        
        const allProfiles = await Profile.findAll(optionAll);
        const pageCount = Math.ceil(allProfiles.length / row);
        
        const options = {
            attributes : ['id', 'user_id', 'user_country', 'user_rank', 'gold_amount'],
            order : [['id', 'ASC']],
            limit : row,
            offset : page
        }
        const someProfiles = await Profile.findAll(options);
        
        res.render('profiles/dashboardProfile', {
            status : 'success',
            profiles : someProfiles,
            page, 
            row,
            pageCount,
            port
        });
    }catch(err){
        throw new Error('cannot get all user')
    }
}

const addNewProfile = (req, res) => {
    try{
        res.render('profiles/addNewProfile');
    }catch(err){
        console.log(err);
        throw new Error('cannot route to addNewProfile');
    }
}

const getProfileById = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            where : {
                id : id
            }
        }
        const profile = await Profile.findOne(options);
        res.render('profiles/getProfileById', {
            profile
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get profile by Id');
    }
}

const createProfile = async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('profiles/addNewProfile', {
                errors : errors.array()
            });
        }else{
            const { user_id, user_country, user_rank, gold_amount } = req.body;
            const option = {
                where : {
                    id : user_id
                }
            }
            const profilesById = await Profile.findAll(option);
            if(profilesById.length == 0){
                res.redirect(400,'/profiles');
            }else{
                await Profile.create({
                user_id,
                user_country,
                user_rank,
                gold_amount,
                createdAt : new Date(),
                updatedAt : new Date()
            });
            res.redirect('/profiles');
            }
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot create profile')
    }
}

const editProfile = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            attributes : ['id', 'user_id', 'user_country', 'user_rank', 'gold_amount' ],
            where : {
                id : id
            }
        }
        const profile = await Profile.findOne(options);
        res.render('profiles/updateProfile', {
            status : 'success',
            profile
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get update user');
    }

}

const updateProfile = async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const { id } = req.params;
            const options = {
                attributes : ['id', 'user_id', 'user_country', 'user_rank', 'gold_amount'],
                where : {
                    id : id
                }
            }
            const profile = await Profile.findOne(options);
            res.render('profiles/updateProfile', {
                errors : errors.array(),
                profile
            });
        }else{
            const { user_id, user_country, user_rank, gold_amount } = req.body;
            await Profile.update(
                {
                    user_id,
                    user_country,
                    user_rank,
                    gold_amount,
                    updatedAt : new Date()
                },
                {
                    where : { id : req.params.id }
                }
            );
            res.redirect('/profiles');
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot update profile');
    }
}

const deleteProfile = async(req, res) => {
    try{
        const { id } = req.params;
        await Profile.destroy({
            where : {
                id : id
            }
        })
        res.redirect('/profiles');
    }catch(err){
        console.log(err);
        throw new Error('cannot delete user');
    }
}

module.exports = {
    getAllProfiles,
    addNewProfile,
    getProfileById,
    createProfile,
    editProfile,
    updateProfile,
    deleteProfile
}

