const { User, Profile, History } = require('../models');
const port = process.env.PORT;
const { validationResult, check } = require('express-validator')

const getAllUsers = async(req, res) => {
    try{
        let { page, row } = req.query;
        if(row == 0 || !page || !row){
            page = 1;
            row = 5;
        }
        page -= page > 0 ? 1 : 0;
        page *= row;
        const optionAll = {
            attributes : ['id', 'username', 'password', 'email'],
            order : [['id', 'ASC']]
        }
        
        const allUsers = await User.findAll(optionAll);
        const pageCount = Math.ceil(allUsers.length / row);
        
        const options = {
            attributes : ['id', 'username', 'password', 'email'],
            order : [['id', 'ASC']],
            limit : row,
            offset : page
        }
        const someUsers = await User.findAll(options);
        
        res.render('users/dashboardUser', {
            status : 'success',
            users : someUsers,
            page, 
            row,
            pageCount,
            port
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get all user');
    }
}

const addNewUser = (req, res) => {
    try{
        res.render('users/addNewUser');
    }catch(err){
        console.log(err);
        throw new Error('cannot route to addNewUser');
    }
}

const getUserById = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            where : {
                id : id
            }
        }
        const user = await User.findOne(options);
        res.render('users/getUserById', {
            user
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot get user by Id');
    }
}

const createUser = async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('users/addNewUser', {
                errors : errors.array()
            });
        }else{
            const { username, password, email } = req.body;
            await User.create({
                username,
                password,
                email,
                createdAt : new Date(),
                updatedAt : new Date()
            });
            res.redirect('/users');
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot create user');
    }
    
}

const editUser = async(req, res) => {
    try{
        const { id } = req.params;
        const options = {
            attributes : ['id', 'username', 'password', 'email'],
            where : {
                id : id
            }
        }
        const user = await User.findOne(options);
        res.render('users/updateUser', {
            status : 'success',
            user
        });
    }catch(err){
        console.log(err);
        throw new Error('cannot go to editUser');
    }

}

const updateUser = async(req, res) => {
    try{
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const { id } = req.params;
            const options = {
                attributes : ['id', 'username', 'password', 'email'],
                where : {
                    id : id
                }
            }
            const user = await User.findOne(options);
            res.render('users/updateUser', {
                errors : errors.array(),
                user
            });
        }else{
            const { username, password, email } = req.body;
            await User.update(
                {
                    username,
                    password,
                    email,
                    updatedAt : new Date()
                },
                {
                    where : { id : req.params.id }
                }
            );
            res.redirect('/users');
        }
    }catch(err){
        console.log(err);
        throw new Error('cannot update user');
    }
}

const deleteUser = async(req, res) => {
    try{
        const { id } = req.params;
        await User.destroy({
            where : {
                id 
            }
        });
        await Profile.destroy({
            where : {
                user_id : id
            }
        });
        await History.destroy({
            where : {
                user_id : id
            }
        });
        res.redirect('/users');
    }catch(err){
        console.log(err);
        throw new Error('cannot delete user');
    }
}

module.exports = {
    getAllUsers,
    addNewUser,
    getUserById,
    createUser,
    editUser,
    updateUser,
    deleteUser
}

