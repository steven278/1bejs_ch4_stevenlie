const express = require('express');
const router = express.Router();
const { check } = require('express-validator')
const { getAllUsers, addNewUser, editUser, getUserById, createUser, updateUser, deleteUser } = require('../controllers/users.controller');

router.get('/', getAllUsers);
router.get('/add', addNewUser);
router.get('/:id', getUserById);
router.get('/:id/update', editUser);
router.post('/', 
    [check('email').isEmail().withMessage('invalid email input'),
    check('password').isLength({ min: 6 }).withMessage('must be at least 5 chars long')],
    createUser);
router.put('/:id', [check('email').isEmail().withMessage('invalid email input'),
    check('password').isLength({ min: 6 }).withMessage('must be at least 5 chars long')],
    updateUser);
router.delete('/:id', deleteUser);

module.exports = router;