const express = require('express');
const router = express.Router();
const { check } = require('express-validator')
const { getAllProfiles, addNewProfile, editProfile, getProfileById, createProfile, updateProfile, deleteProfile } = require('../controllers/profiles.controller');

router.get('/', getAllProfiles);
router.get('/add', addNewProfile);
router.get('/:id', getProfileById);
router.get('/:id/update', editProfile);
router.post('/', createProfile);
router.put('/:id', updateProfile);
router.delete('/:id', deleteProfile);

module.exports = router;