const express = require('express');
const router = express.Router();
const { check } = require('express-validator')
const { getAllHistories, addNewHistory, editHistory, getHistoryById, createHistory, updateHistory, deleteHistory } = require('../controllers/histories.controller');

router.get('/', getAllHistories);
router.get('/add', addNewHistory);
router.get('/:id', getHistoryById);
router.get('/:id/update', editHistory);
router.post('/', createHistory);
router.put('/:id', updateHistory);
router.delete('/:id', deleteHistory);

module.exports = router;