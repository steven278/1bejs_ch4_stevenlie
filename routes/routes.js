const express = require('express');
const router = express.Router();
const userRoutes = require('./users.routes');
const profileRoutes = require('./profiles.routes');
const historyRoutes = require('./histories.routes');
const { Admin } = require('../models');


const admin = {
    email : 'steve@mail.com',
    password : '123456'
}

const checkDbAdmin = async (req, res, next) => {
    const authenticatedAdmin =  await Admin.findAll();
    const { email, password } = authenticatedAdmin[0];
    console.log(admin.email, email);
    if(admin.email == email && admin.password == password){
        next();
    }else{
        res.render('home')
    }
}

router.use(checkDbAdmin)

router.get('/', (req, res) => {
    res.render('home');
})

router.use('/users', userRoutes);
router.use('/profiles', profileRoutes);
router.use('/histories', historyRoutes);

router.get('*', (req, res) => {
    res.status(404).render('error',{
        status : 404,
        message : 'Page not found'
    });
});

module.exports = { router }