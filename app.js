require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT;
const { router } = require('./routes/routes');
const methodOverride = require('method-override');
const morgan = require('morgan');

app.use(express.json());
app.use(express.urlencoded( { extended : true } ))
app.use(methodOverride('_method'));
app.set('view engine', 'ejs');

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use(router);


app.use((err, req, res, next) => {
    console.error(err.stack);
    // res.status(400).send('sth wrong')
    res.status(500).render('error', {
        status : 500,
        message :  err.message
    })
});

app.listen(port, () => {
    console.log(`App is listening on port ${port}`);
});

